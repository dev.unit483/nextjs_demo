const TodoList = ({todoList, handleDelete}) => {
    return (
        <>
            <h1>Список дел</h1>
            {todoList.length > 0 ? (
                <ul>
                {todoList.map(todo =>
                    <li key={todo.id}>
                        {todo.text}
                        <button onClick={(e) => {
                            e.preventDefault()
                            handleDelete(todo)
                        }}>delete</button>
                    </li>
                )}
            </ul>
            ) : (
                "Список пока что пуст"
            )}
        </>
    )
};
export default TodoList
