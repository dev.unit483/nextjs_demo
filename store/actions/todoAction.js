import { CREATE_TODO, DELETE_TODO } from '../constants/constants'

export const createTodoAction = (payload) => ({
  type: CREATE_TODO,
  payload,
});

export const deleteTodoAction = (payload) => ({
  type: DELETE_TODO,
  payload,
});