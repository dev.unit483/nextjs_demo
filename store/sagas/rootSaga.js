import { all, fork } from 'redux-saga/effects';
import {sagaWatcherCreateTodo, sagaWatcherDeleteTodo} from './todoSaga';

export default function* rootSaga() {
  yield all([
    fork(sagaWatcherCreateTodo),
    fork(sagaWatcherDeleteTodo),
  ]);
}
