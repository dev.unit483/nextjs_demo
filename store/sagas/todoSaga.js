import {call, takeEvery} from 'redux-saga/effects';
import axios from 'axios';
import { CREATE_TODO, DELETE_TODO } from '../constants/constants';
import HOST from '../../config';

function* createTodoRequest(payload) {
  return yield call(axios, `${HOST}/todo/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: payload,
  });
}

export function* sagaWorkerCreateTodo(action) {
  const response = yield createTodoRequest(action.payload);
  // if (+response.status === 201) {
  //       write to store
  // }
}

export function* sagaWatcherCreateTodo() {
  yield takeEvery(CREATE_TODO, sagaWorkerCreateTodo);
}

function* deleteTodoRequest(payload) {
  console.log(payload)
  return yield call(axios, `${HOST}/todo/`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: payload,
  });
}

export function* sagaWorkerDeleteTodo(action) {
  yield deleteTodoRequest(action.payload);
}

export function* sagaWatcherDeleteTodo() {
  yield takeEvery(DELETE_TODO, sagaWorkerDeleteTodo);
}