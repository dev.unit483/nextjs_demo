import CustomLink from "./CustomLink";
import Head from "next/head";
import styles from "../styles/NavContainer.module.scss"

export const NavContainer = ({children, keywords}) => {
    return (
        <>
            <Head>
                <meta keywords={keywords}></meta>
            </Head>
            <div className={styles.navbar}>
                <CustomLink href={"/"} text={"Главная"}/>
                <CustomLink href={"/todoList"} text={"НеГлавная"}/>
            </div>
            <div>
                {children}
            </div>
        </>
    )
}