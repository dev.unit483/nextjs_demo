import { NavContainer } from "../components/NavContainer"
import { useState } from 'react'
import TodoList from "../components/TodoList";
import { createTodoAction, deleteTodoAction } from "../store/actions/todoAction";
import { useDispatch } from "react-redux";
import HOST from "../config";
import { getUniqueId } from "../store/utils/getUniqueId";


const Index = ({serverTodoList}) => {
    const [todoTextInput, setTodoTextInput] = useState('')
    const [todoList, setTodoList] = useState(serverTodoList)
    const dispatch = useDispatch()

    const handleChangeText = (e) => {
        e.preventDefault()
        setTodoTextInput(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        const newTodo = {id: getUniqueId(todoList), text: todoTextInput}

        setTodoList([
            newTodo, ...todoList
        ])
        setTodoTextInput('')
        dispatch(createTodoAction(newTodo));
    }

    const handleDelete = (todo) => {
        const updatedTodoList = todoList.filter(todoItem => todoItem.id !== todo.id)
        setTodoList(updatedTodoList)
        dispatch(deleteTodoAction(todo));
    }

    return (
        <NavContainer keywords={"lolkeywords"}>
            <form>
                <input placeholder="enter text" value={todoTextInput} type="text" onChange={handleChangeText}/>
                <button onClick={handleSubmit}>submit</button>
            </form>
            <TodoList todoList={todoList} handleDelete={handleDelete}/>
        </NavContainer>
    )
};
export default Index

export async function getStaticProps(context) {
    const response = await fetch(`${HOST}/todo/`)
    const serverTodoList = await response.json()
    return {
        props: {serverTodoList},
    }
}

