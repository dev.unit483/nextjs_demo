const express = require("express")
const router = express.Router();
const fs = require('fs');

router.get("/todo", (req, res) => {
    function getTodoList(callback) {
    }
    getTodoList(fs.readFile('todoList.json', function (err, data) {
        res.end(JSON.stringify(JSON.parse(data)))
    }))
});

router.post("/todo", (req, res) => {
    fs.readFile('todoList.json', function (err, data) {
        const json = JSON.parse(data)
        json.unshift(req.body)
        fs.writeFileSync("todoList.json", JSON.stringify(json))
    })
    res.end('Создали')
});

router.delete("/todo", (req, res) => {
    fs.readFile('todoList.json', function (err, data) {
        const filteredJson = JSON.parse(data).filter(todoItem => todoItem.id !== req.body.id)
        fs.writeFileSync("todoList.json", JSON.stringify(filteredJson))
    })
    res.end('Удалили')
});


module.exports = router;
