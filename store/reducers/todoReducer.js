import { REQUEST_TODO } from '../constants/constants';

const initialState = {
  todo_list: []
};

function TodoReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_TODO:
      return {
        ...state,
        todo_list: action.payload,
      };
    default:
      return { ...state };
  }
}

export default TodoReducer;
