import { NavContainer } from "../components/NavContainer"
import HOST from "../config";


const TodoList = ({serverTodoList}) => {

    return (
        <NavContainer keywords={"another key words"}>
            {serverTodoList.length > 0 ? (
                <ul>
                {serverTodoList.map(todo =>
                    <li key={todo.id}>
                        {todo.text + ' example'}
                    </li>
                )}
            </ul>
            ) : (
                "Список пока что пуст и на второй странице тоже"
            )}
        </NavContainer>
    )
};
export default TodoList

export async function getStaticProps(context) {
    const response = await fetch(`${HOST}/todo/`)
    const serverTodoList = await response.json()
    return {
        props: {serverTodoList},
    }
}

